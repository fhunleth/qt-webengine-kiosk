#-------------------------------------------------
#
# Project created by QtCreator 2010-07-22T01:54:24
#
#-------------------------------------------------

QT       = core gui network widgets multimedia webenginewidgets

CONFIG += console link_pkgconfig c++11
TARGET = qt-webengine-kiosk
TEMPLATE = app

CONFIG(debug, debug|release) {
# here comes debug specific statements
    message(Debug build)
} else {
# here comes release specific statements
    CONFIG -= debug
    DEFINES += QT_NO_DEBUG_OUTPUT
}

!win32 {
    isEmpty( PREFIX ) {
        PREFIX = /usr/local
    }
}

message(Qt version: $$[QT_VERSION])
message(Qt is installed in $$[QT_INSTALL_PREFIX])
message(Settings:)
message(- QT     : $${QT})
message(- PREFIX : $${PREFIX})
message(- TARGET : $${TARGET})

SOURCES += main.cpp\
    mainwindow.cpp \
    webview.cpp \
    qplayer.cpp \
    KioskSettings.cpp

HEADERS  += mainwindow.h \
    webview.h \
    qplayer.h \
    KioskSettings.h

# DEBUG
#message(- SOURCES: $${SOURCES})
#message(- HEADERS: $${HEADERS})

# INSTALL

target.path = $${PREFIX}/bin

INSTALLS += target

RESOURCES += \
    ui.qrc
